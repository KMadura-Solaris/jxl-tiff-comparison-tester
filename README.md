# JPEG-XL tester

This tool gives us a way to check if JPEG-XL lossless-ly compressed images differ in any way from an original TIFF images.
Since default libjxl tools don't even accept TIF format, it is necessary to convert original image into intermediary format like PNG if we want to use original tools (cjxl for example) to convert an image into JXL.

TIFF has unique feature called "pages" which means a single file can store multiple pages.
Most image viewers do not implement this function, however some specialized software like IrfanView does.

IrfanView doesn't have any precise tool (or at least I couldn't find any option) to check differences between two images.
We can take a look at parameters like resolution, bit-depth, uncompressed size or histogram, but it still doesn't tell us if both images are pixel to pixel perfect. There is indeed an option in IrfanView to show a preview window with all the differences but there is still a chance we may not see that one single different pixel.

To make a precise comparison between TIFF and JXL, I had to create this tool which generates sha256 on raw uncompressed images and compares them.
If these hashes are equal we can certainly be sure both images are absolutely equal (not just >99% identical).

### Usage:

* `-t, --tiff` tiff input file
* `-x, --jxl` jpeg-xl input file
* `-p, --page` (optional) specific page of TIFF image we want to compare with JXL

### Results:

```text
./jxl_tiff_tester.py -t example.tiff -x example.jxl -p 0
Counting TIFF pages
File: example.tiff
File size: 385118908 Bytes
Page 0
 Resolution: 5760x4092
 Bit depth: 8
 Compression: tiff_lzw
 SHA256: 448ae06d4eac2a26fb4f57305e18bfce54f7b320cc020e69afd49c586e0a02f1

Analyzing JPEG-XL file
File: example.jxl
File size: 7558106 Bytes
 Resolution: 5760x4092
 Bit depth: 8
 Compression: likely lossless*
 SHA256: 448ae06d4eac2a26fb4f57305e18bfce54f7b320cc020e69afd49c586e0a02f1

Result: [POSITIVE] Both images are equal

* - JXLpy doesn't provide exact information on image compression
```

As we can see, an example TIFF image provided by CRYO-EM (electron cryo-microscopes in Solaris) and compressed JPEG-XL image of its first TIFF page are equal.

This shows JPEG-XL compression doesn't break an image in any way. If it does, please do appropriate tests and let me know.

These results were generated on x86_64 Intel i7 CPU on Ubuntu 22.04 running on WSL (Windows 10).
There is a possibility running this script on other cpu architectures (ARM for example) could result in false hashes, therefore pixel by pixel comparison may be required.

### Requirements:

All required python3 packages are listed in both python files.

Please install [JXLpy](https://github.com/olokelo/jxlpy) from pip or from source as it is absolutely required.

This script requires full installation of [libjxl](https://github.com/libjxl/libjxl). Please make sure all required and most of the optional packages are installed before compiling libjxl as this may break or limit both [libjxl](https://github.com/libjxl/libjxl) and [JXLpy](https://github.com/olokelo/jxlpy) functionality.

### Encountered issues:

JPEG-XL format and compression are stable which means image compression and decompression will not change, however libjxl is still in development and while it does produce or decode a proper JPEG-XL images, it is still not mature enough in my opinion (2023.03.10).

In some places in its source code you can still find TODOs asking for potential performance improvements, implementing additional functions (error handling for example) or creating better documentation.

JXLpy does have its issues which shouldn't affect compressed or decompressed images.
JXL's Pillow plugin is unstable, that's why I had to use Pillow for TIFF decoding and JXLpy functions for JPEG-XL.

---

Owners of "example.tiff" file gave a permission to use this image for compression, testing and benchmarking purposes including sharing it in this publicly available repository.

---

Author: [Krzysztof Madura](mailto:krzysztof.madura@uj.edu.pl), [Solaris Synchrotron](https://synchrotron.uj.edu.pl/)