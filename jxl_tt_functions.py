import hashlib
import jxlpy
import sys
from PIL import Image


#
# Decoding and comparison functions
#

# Fetch information about particular image/page
def tiff_get(image_tiff, page):
    image_tiff.seek(page)

    # bit-depth - Pillow here doesn't directly tell us what bit depth is, we have to guess
    mode_to_bpp = {'1': 1, 'L': 8, 'P': 8, 'RGB': 24, 'RGBA': 32, 'CMYK': 32, 'YCbCr': 24, 'I': 32, 'F': 32}
    bit_depth = 0 if image_tiff.mode not in mode_to_bpp.keys() else mode_to_bpp[image_tiff.mode]

    # image compression
    compression = "unknown or uncompressed" if "compression" not in image_tiff.info else image_tiff.info['compression']

    # sha256
    sha256_sum = hashlib.sha256(image_tiff.tobytes()).hexdigest()

    return {
        "resolution": (image_tiff.width, image_tiff.height),
        "depth": bit_depth,
        "compression": compression,
        "sha256": sha256_sum
    }


# Fetch information about this image
def jxl_get(image_jxl):
    jxl_info = image_jxl['object'].get_info()

    # bit depth
    bit_depth = jxl_info['bits_per_sample']

    # image compression (JXLpy doesn't provide exact information)
    compression = 'likely lossless*' if jxl_info['uses_original_profile'] == 1 else 'likely lossy*'

    # sha256
    sha256_sum = hashlib.sha256(image_jxl['frame']).hexdigest()

    return {
        "resolution": (jxl_info['xsize'], jxl_info['ysize']),
        "depth": bit_depth,
        "compression": compression,
        "sha256": sha256_sum
    }


# Opening TIFF files
def tiff_open(tiff_file):
    return Image.open(tiff_file)


# Opening JXL files
def jxl_open(jxl_file):
    file = open(jxl_file, 'rb')
    file_read = file.read()
    jxlpy_object = jxlpy.JXLPyDecoder(file_read)
    jxlpy_frame = jxlpy_object.get_frame()
    # Frame has to be decoded here and file.read can't be pushed
    # directly as an argument into JXLPyDecoder, otherwise
    # everything breaks. Btw. Pillow plugin for JXL is even
    # more unstable.

    return {
        'object': jxlpy_object,
        'frame': jxlpy_frame
    }


# List TIFF pages
def tiff_pages(image_tif, selected_page):
    # Single page mode
    if selected_page >= 0:
        return [selected_page]

    # Multiple pages mode
    return range(image_tif.n_frames)


# List JXL pages - leaving it for eventual future use
def jxl_pages(image_jxl):
    jxl_info = image_jxl['object'].get_info()

    if jxl_info['have_animation'] == 0:
        return [0]

    if jxl_info['animation']['tps_numerator'] <= 0:
        return [0]

    return range(jxl_info['animation']['tps_numerator'])


#
# Validation and other functions
#

# Check if a tiff file has user specified page
def tiff_validate_page(image_tif, selected_page):
    if selected_page >= 0:
        if image_tif.n_frames <= selected_page:
            print("Warning!! Selected page %d does not exist, defaulting to first page\n" % selected_page)
            return 0

    return selected_page


# Suppress traceback on Ctrl+C
def suppress_traceback():
    def _no_traceback_excepthook(exc_type, exc_val, traceback):
        if isinstance(exc_val, KeyboardInterrupt):
            return
        sys.__excepthook__(exc_type, exc_val, traceback)

    sys.excepthook = _no_traceback_excepthook
