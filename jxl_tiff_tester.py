#!/usr/bin/env python3

# Author: Krzysztof Madura, Solaris Synchrotron
# License: MIT
# A tool to measure differences between TIFF and JXL files

# External imports
import argparse
import os.path
from os.path import exists

# Local imports
import jxl_tt_functions as ttfunc


# Suppress traceback after ctrl-c
ttfunc.suppress_traceback()


# Arguments setup
args_parser = argparse.ArgumentParser('jxl_tiff_tester', 'Testing differences between TIFF and JXL')
# TIFF input file
args_parser.add_argument('-t', '--tiff', required=True)
# JXL input file
args_parser.add_argument('-x', '--jxl', required=True)
# Select page from multi-page TIFF file
args_parser.add_argument('-p', '--page', required=False, type=int, default=-1)
args = args_parser.parse_args()

# Parse arguments
if args.page < -1:
    args.page = -1

if not exists(args.tiff):
    print("A tiff format file is required")
    exit(1)

if not exists(args.jxl):
    print("A jpeg-xl format file is required")
    exit(1)

#
# TIFF section
#

image_tif = ttfunc.tiff_open(args.tiff)
image_tif_pages = {}

# If selected page by user doesn't exist, default to page number 0
# This has no effect in multipage mode when args.page is less than 0
args.page = ttfunc.tiff_validate_page(image_tif, args.page)

print("Counting TIFF pages")
print("File:", args.tiff)
print("File size: %d Bytes" % os.path.getsize(args.tiff))

for page in ttfunc.tiff_pages(image_tif, args.page):
    tif_object = ttfunc.tiff_get(image_tif, page)

    image_tif_pages[page] = tif_object

    print("Page %d" % page)
    print(" Resolution: %dx%d" % tif_object['resolution'])
    print(" Bit depth: %d" % tif_object['depth'])
    print(" Compression:", tif_object['compression'])
    print(" SHA256:", tif_object['sha256'])
print() # newline

#
# JPEG-XL section
#

image_jxl = ttfunc.jxl_open(args.jxl)

print("Analyzing JPEG-XL file")
print("File:", args.jxl)
print("File size: %d Bytes" % os.path.getsize(args.jxl))

jxl_object = ttfunc.jxl_get(image_jxl)

print(" Resolution: %dx%d" % jxl_object['resolution'])
print(" Bit depth: %d" % jxl_object['depth'])
print(" Compression:", jxl_object['compression'])
print(" SHA256:", jxl_object['sha256'])
print() # newline

#
# Checking if images are equal
#

# Single page mode
if args.page >= 0:
    if image_tif_pages[args.page]['sha256'] == jxl_object['sha256']:
        print("Result: [POSITIVE] Both images are equal")
    else:
        print("Result: [NEGATIVE] Images are different")

# Multiple pages mode
else:
    number_of_equal_images = 0

    for page in image_tif_pages:
        if image_tif_pages[page]['sha256'] == jxl_object['sha256']:
            print("Found match at TIFF page", page)
            number_of_equal_images += 1

    if number_of_equal_images == 0:
        print("Result: [NEGATIVE] none of the images are equal with JXL one")
    else:
        print("Result: [POSITIVE] number of found equal images between TIFF and JXL:", number_of_equal_images)

print()
print("* - JXLpy doesn't provide exact information on image compression")
